#Registration Digistore Thank You Page

## TL;DR:
Sell registrations / roles in Drupal through [digistore24](https://www.digistore24.com/) products. Provides a separate **Drupal user registration page** (e.g. "user/register-digistore24" (editable)) which acts as **thank you page for digistore24 sales** and allows to set roles for valid registrations.

## Functionality:
*   Provides a separate registration page with individual URL (defaults to "user/register-digistore24") based on the default registration form elements
*   Allows to set thank you page key via form (admin/config/people/registration-digistore-typ). Validates each submission using this page key. Prevents registration if validation fails and hides the form with an error message.
*   Allows to set role(s) for successfully registered digistore users automatically (admin/config/people/registration-digistore-typ)
*   Stores the digistore24 order ID in a custom user profile field (text) and on form submit validates the order id to be unique, otherwise prevents registration
*   Validates & decodes the encoded GET parameters (See https://docs.digistore24.com/knowledge-base/about-the-thank-you-page-key/?lang=en)
*   Allows to alter the form programmatically (by using [hook_form_FORM_ID_alter()](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_form_FORM_ID_alter/7.x))
*   Allows to alter digistore parameters by implementing HOOK_get_digistore_query_parameter_keys_alter.
*   Does NOT care for ending subscription, recurring, etc. - just handles registration. Feel free to add patches or submodules to extend the functionality.

## Installation
*   Download and install the module
*   Create a text field (varchar 255, for example called "digistore24_order_id") as user profile field (_admin/config/people/accounts/fields_). Optionally you may want to use [Field Permissions](https://www.drupal.org/project/field_permissions) to hide the field entirely or only the edit display for users.
*   Set the required module settings at _admin/config/people/registration-digistore-typ_ and compare them to your digistore settings.
*   Create a product at digistore24 and set the form url as "Thank you page" and "Second Thank you page"

## Dependencies
*   No contrib module dependencies

## Modifications / Hooks / Individualization
*   Allows to alter digistore parameters by implementing HOOK_get_digistore_query_parameter_keys_alter.
*   Allows to alter the form programmatically (by using hook_form_FORM_ID_alter()). Custom actions / validations can be made in validate / submit hooks added there.

## Development proudly sponsored by German Drupal Friends & Companies:
[webks: websolutions kept simple](https://www.webks.de) (https://www.webks.de) and [DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe](https://www.drowl.de) (https://www.drowl.de)
