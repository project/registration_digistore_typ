<?php

/**
 * @file Administration form for registration_digistore_typ.
 */

/**
 * registration_digistore_typ admin settings form.
 */
function registration_digistore_typ_settings_form($form, &$form_state) {
  $form['registration_digistore_typ_digistore24_page_key'] = array(
    '#type' => 'textfield',
    '#title' => t('digistore24 Thank you page key'),
    "#description" => t('The digistore24 Thank you page key. You can find (or change) it in your digistore24 account details settings.'),
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_digistore24_page_key', ''),
  );
  $form['registration_digistore_typ_reg_roles'] = array(
    '#type' => 'select',
    '#title' => t('Roles for digistore24 registrants'),
    "#description" => t('Select the roles to automatically set for users from digistore24 who registered successfully.'),
    '#options' => user_roles(TRUE, NULL),
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_reg_roles', ''),
  );
  $user_profile_fields = field_info_instances('user', 'user');
  foreach($user_profile_fields as $key => $field_info){
    $user_profile_fields[$key] = check_plain($field_info['label']) . ' (' . $key . ')';
  }
  $form['registration_digistore_typ_user_ds24_order_id_field'] = array(
    '#type' => 'select',
    '#title' => t('User profile digistore24 Order ID field'),
    "#description" => t('Select the !userprofile field which stores the digistore24 order_id automatically after registration. This field is also used to ensure each registration has a unique order id.', array('!userprofile' => l('user profile', 'admin/config/people/accounts/fields'))),
    '#options' => $user_profile_fields,
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_user_ds24_order_id_field'),
  );
  $form['registration_digistore_typ_digistore24_encryption_type'] = array(
    '#type' => 'select',
    '#title' => t('Encrypt data in thank you URL'),
    "#description" => t('Select the SAME option for "Encrypt data in thank you URL" as you selected in your product at digistore24.'),
    '#options' => array(
      'none' => 'No, transfer data in plain language',
      'private' => 'Yes, encrypt personal data',
      'all' => 'Yes, encrypt all data'),
    '#multiple' => FALSE,
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_digistore24_encryption_type', 'none'),
  );
  $form['registration_digistore_typ_form_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration form path'),
    "#description" => t('The registration form path. Default value: user/register-digistore. This should be set as "Thank you page" and "Second thank you page" values (full URL) on digistore24.'),
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_form_path', 'user/register-digistore24'),
  );
  $form['registration_digistore_typ_form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration form title'),
    "#description" => t('The title of the registration form page. If let empty, uses Drupal default.'),
    '#required' => FALSE,
    '#default_value' => variable_get('registration_digistore_typ_form_title'),
  );
  $form_description = variable_get('registration_digistore_typ_form_description', array('value' => 'The debit will be performed by Digistore24.com', 'format' => 'full_html'));
  $form['registration_digistore_typ_form_description'] = array(
    '#type' => 'text_format',
    '#base_type' => 'textarea',
    '#format' => 'full_html',
    '#title' => t('Registration form description'),
    "#description" => t('The description of the registration form page, displayed above the form. By default contains the digistore24 default text: "The debit will be performed by Digistore24.com"'),
    '#required' => FALSE,
    '#default_value' => $form_description['value'],
  );
  $form['registration_digistore_typ_form_submit_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit button text'),
    "#description" => t('The text of the form submit button.'),
    '#required' => FALSE,
    '#default_value' => variable_get('registration_digistore_typ_form_submit_button_text', 'Submit'),
  );
  $form['registration_digistore_typ_prefill_email'] = array(
    '#type' => 'select',
    '#title' => t('Prefill email adress with email value from digistore24'),
    "#description" => t('Set the registration form email address field to the value entered at digistore24.'),
    '#options' => array(
      '0' => t('No'),
      '1' => t('Yes'),
      '2' => t('Yes') . ' & ' . t('Disabled')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_prefill_email', '0'),
  );
  $form['registration_digistore_typ_prefill_username'] = array(
    '#type' => 'select',
    '#title' => t('Prefill username with email value from digistore24'),
    "#description" => t('Set the registration form email address field to the value entered at digistore24.'),
    '#options' => array(
      '0' => t('No'),
      '1' => t('Yes'),
      '2' => t('Yes') . ' & ' . t('Disabled')
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('registration_digistore_typ_prefill_username', '0'),
  );

  $form['#submit'][] = 'registration_digistore_typ_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Administration form validation.
 */
function registration_digistore_typ_settings_form_validate($form, &$form_state) {
  global $base_root;
  $registration_form_path = $form_state['values']['registration_digistore_typ_form_path'];
  if (substr($registration_form_path, 0, 1) == '/'  || !filter_var($base_root . '/' . $registration_form_path, FILTER_VALIDATE_URL)){
    form_set_error('registration_digistore_typ_form_path', t('This is not a valid path.'));
  }
}

/**
 * Administration form submission.
 */
function registration_digistore_typ_settings_form_submit($form, &$form_state) {
  if($form_state['values']['registration_digistore_typ_form_path'] != variable_get('registration_digistore_typ_form_path')){
    // Patch changed, menu needs rebuild:
    variable_set('menu_rebuild_needed', TRUE);
    drupal_set_message(t('Path changed, clear caches.'), 'status');
    drupal_flush_all_caches();
  }
}
