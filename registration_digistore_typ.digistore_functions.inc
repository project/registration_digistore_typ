<?php

/**
 * @file Contains digistore private vendor functions prefixed with module name: _registration_digistore_typ_[...].
 * For original sources see:
 * - https://docs.digistore24.com/knowledge-base/transfer-of-order-data-to-the-thank-you-page-url-as-get-parameter/?lang=en
 * -
 */

// Do not change. Values given from digistore24.
define('DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX',    'ds24');
define('DS24_ARRAY_ENCRYPTPTION_VALIDATION_CHAR_COUNT', 5);

/**
 * Helper function provided by digistore24.
 *
 * @param string $string
 * @param string $substring
 * @return boolean
 */
function _registration_digistore_typ_digistore_string_starts_with($string, $substring)
{
    if ($substring === '') {
        return true;
    }

    if (strlen($string) < strlen($substring)) {
        return false;
    }


    if ($string[0] != $substring[0]) {
        return false;
    }

    return substr($string, 0, strlen($substring)) === $substring;
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param string $plain_text
 * @return string
 */
function _registration_digistore_typ_digistore_encrypt($secret_key, $plain_text)
{
    if (empty($secret_key)) {
        $secret_key = DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX;
    }

    $encrypt_method = "AES-256-CBC";

    $key = hash('sha256', $secret_key);

    $iv = random_bytes( 16 );

    $output = openssl_encrypt( $plain_text, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);

    $output = str_replace( array( '=', '+' ), array( '_e', '_p' ), $output );

    $output = bin2hex($iv ) . '-' . $output;

    return $output;
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param string $encrypted_string
 * @return string
 */
function _registration_digistore_typ_digistore_decrypt($secret_key, $encrypted_string)
{
    $encrypt_method = "AES-256-CBC";

    $secret_iv = $secret_key;
    $key = hash('sha256', $secret_key);

    $encrypted_string = str_replace( array( '_e', '_p' ), array( '=', '+' ), $encrypted_string );

    $is_iv_appended = strlen($encrypted_string) > 33 && $encrypted_string[32] === '-';

    if ($is_iv_appended) {
        $iv = @hex2bin( substr( $encrypted_string, 0, 32 ) );
        $encrypted_string = substr( $encrypted_string, 33 );

        if (empty($iv)) {
            return $encrypted_string;
        }
    }
    else {
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
    }

    $plain_text = openssl_decrypt(base64_decode($encrypted_string), $encrypt_method, $key, 0, $iv);

    return $plain_text;
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param string $plaintext
 * @return string
 */
function _registration_digistore_typ_digistore_encrypt_url_one_arg($secret_key, $plaintext)
{
    if (empty($secret_key)) {
        $secret_key = DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX;
    }

    $len = DS24_ARRAY_ENCRYPTPTION_VALIDATION_CHAR_COUNT;

    $validation_prefix = $secret_key
        ? mb_substr($secret_key, 0, $len)
        : '';

    return DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX . _registration_digistore_typ_digistore_encrypt($secret_key, $validation_prefix . $plaintext);
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param string $enrypted_string
 * @return string
 */
function _registration_digistore_typ_digistore_decrypt_url_one_arg($secret_key, $enrypted_string)
{
    $is_maybe_encrypted = _registration_digistore_typ_digistore_string_starts_with($enrypted_string, DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX);
    if (!$is_maybe_encrypted) {
        return $enrypted_string;
    }

    if (empty($secret_key)) {
        $secret_key = DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX;
    }
    $encrypted = mb_substr($enrypted_string, strlen(DS24_ARRAY_ENCRYPTPTION_VALIDATION_PREFIX));
    $decrypted = _registration_digistore_typ_digistore_decrypt($secret_key, $encrypted);
    $len = DS24_ARRAY_ENCRYPTPTION_VALIDATION_CHAR_COUNT;
    $validation_prefix = $secret_key
        ? mb_substr($secret_key, 0, $len)
        : '';
    $is_valid = $secret_key
        ? _registration_digistore_typ_digistore_string_starts_with($decrypted, $validation_prefix)
        : true;
    return $is_valid
        ? mb_substr($decrypted, mb_strlen($validation_prefix))
        : $enrypted_string;
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param array $array
 * @param mixed $keys_to_encrypt
 * @return array
 */
function _registration_digistore_typ_digistore_encrypt_url_args($secret_key, $array, $keys_to_encrypt = 'all')
{
    foreach ($array as $key => &$value) {
        $must_encrypt = $keys_to_encrypt === 'all' || in_array($key, $keys_to_encrypt);
        if ($must_encrypt) {
            $value = _registration_digistore_typ_digistore_encrypt_url_one_arg($secret_key, $value);
        }
    }

    return $array;
}

/**
 * Helper function provided by digistore24.
 *
 * @param string $secret_key
 * @param array $array
 * @return array
 */
function _registration_digistore_typ_digistore_decrypt_url_args($secret_key, $array)
{
    foreach ($array as $key => &$value) {
        $value = _registration_digistore_typ_digistore_decrypt_url_one_arg($secret_key, $value);
    }

    return $array;
}

/**
 * Helper function provided by digistore24.
 * This signature may be used to validate a Digistore24 ipn request to your server.
 *
 * @param string $secret_key
 * @param array $array
 * @return boolen
 */
function _registration_digistore_typ_digistore_signature($secret_key, $parameters, $convert_keys_to_uppercase = false)
{
    $algorythm           = 'sha512';
    $sort_case_sensitive = true;

    if (!$secret_key) {
        return 'no_signature_passphrase_provided';
    }

    unset($parameters['sha_sign']);
    unset($parameters['SHASIGN']);

    if ($convert_keys_to_uppercase) {
        $sort_case_sensitive = false;
    }

    $keys = array_keys($parameters);
    $keys_to_sort = [];
    foreach ($keys as $key) {
        $keys_to_sort[] = $sort_case_sensitive
            ? $key
            : strtoupper($key);
    }

    array_multisort($keys_to_sort, SORT_STRING, $keys);

    $sha_string = "";
    foreach ($keys as $key) {
        $value = $parameters[$key];

        $is_empty = !isset($value) || $value === "" || $value === false;
        if ($is_empty) {
            continue;
        }

        $upperkey = $convert_keys_to_uppercase
            ? strtoupper($key)
            : $key;

        $sha_string .= "$upperkey=$value$secret_key";
    }

    $sha_sign = strtoupper(hash($algorythm, $sha_string));

    return $sha_sign;
}

/**
 * Returns true if the parameters given exactly match the sha_sign hash value and thereby confirms
 * the request to be a valid order. Otherwise returns false.
 *
 * Important: We have to exactly define the digistore query parameters, because
 * the digistore sha_sign validation logic is based on exactly all parameters.
 * So if one parameter is missing or too much, this will result in failed validation of the hash!
 *
 * @param string $secret_key
 * @param array $parameters
 * @return boolean
 */
function _registration_digistore_typ_digistore_is_valid_request($secret_key, $parameters){
    $sha_sign = $parameters['sha_sign'];
    $expected_signature = _registration_digistore_typ_digistore_signature($secret_key, $parameters);
    return $sha_sign == $expected_signature;
}
