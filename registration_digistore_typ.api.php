<?php

/**
 * @file API functions examples.
*/

/**
 * Alter the digistore query parameters (add / change / remove).
 *
 * Important: We have to exactly define the digistore query parameters, because
 * the digistore sha_sign validation logic is based on exactly all parameters.
 * So if one parameter is missing or too much, this will result in failed validation of the hash!
 *
 * @param [type] $digistore_parameter_keys
 * @return void
 */
function HOOK_get_digistore_query_parameter_keys_alter(&$digistore_parameter_keys){
  $digistore_parameter_keys[] = array(
    'examplekey'
  );
}
